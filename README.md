# Foodo

In this era, many companies and institutions have shifted to the digital
applications to perform their objectives due to their efficiency and speed.
Not only big companies like in the Silicon Valley, small businesses have also
shifted to the digital platform, including restaurants. Introducing Foodo,
a food-ordering framework application for every restaurant.

Customers now can order their desired foods and beverages from the application
directly from the table. They can also directly pay their order after submitting
it by using available digital money, such as OVO or pay before leaving the
restaurant by cash.

From the other side, restaurant owners can load available menus in
their restaurant. For each menu, there are fields for photos, short description,
ingredients, and price. They can also customize the application with their
restaurant color choices and photos to ensure that the application matches
the restaurant’s theme.

This project is a web-based application. A key that is displayed in
the restaurant that changes over time can be used to authenticate the customers
really exist in the restaurant.

## Documents

[Technical Documents](https://docs.google.com/document/d/1L61B1r6iajxGm0yopr8qoF7LGlDrFYV0YFrydWK96O0/edit?usp=sharing)

[Worklog](https://docs.google.com/spreadsheets/d/1QNQkBhpKAyYHC4QgXKAGhkzkHTTMhn_Awt9Z31FoKrk/edit?usp=sharing)

[Diagrams](https://app.lucidchart.com/invitations/accept/f298740f-68e8-41e2-9529-bbf615325c01)

[Mockup](https://www.figma.com/file/f5cT4htUNhBU4mWQjeeSoF/RPL-Individual-Project?node-id=0%3A1)

## Owner
Figo Muhammad Alfaritzi

1806241072