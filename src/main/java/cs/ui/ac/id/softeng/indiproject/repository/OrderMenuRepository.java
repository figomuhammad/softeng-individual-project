package cs.ui.ac.id.softeng.indiproject.repository;

import cs.ui.ac.id.softeng.indiproject.model.OrderMenu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderMenuRepository extends JpaRepository<OrderMenu, Long> {
}
