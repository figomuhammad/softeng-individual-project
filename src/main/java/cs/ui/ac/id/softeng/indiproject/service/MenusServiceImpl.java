package cs.ui.ac.id.softeng.indiproject.service;

import cs.ui.ac.id.softeng.indiproject.model.Menu;
import cs.ui.ac.id.softeng.indiproject.model.Order;
import cs.ui.ac.id.softeng.indiproject.model.OrderMenu;
import cs.ui.ac.id.softeng.indiproject.repository.MenuRepository;
import cs.ui.ac.id.softeng.indiproject.repository.OrderMenuRepository;
import cs.ui.ac.id.softeng.indiproject.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class MenusServiceImpl implements MenusService {

    private Order order = new Order();

    private Set<OrderMenu> orderMenus = new HashSet<>();

    @Autowired
    private OrderMenuRepository orderMenuRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private MenuRepository menuRepository;

    @Override
    public void addMenu(long id, int quantity) {
        Menu menu = menuRepository.findById(id);
        OrderMenu orderMenu = new OrderMenu(order, menu, quantity);
        orderMenus.add(orderMenu);
    }

    @Override
    public void deleteMenu(long id) {
        for (OrderMenu orderMenu : orderMenus) {
            if (orderMenu.getId() == id) {
                orderMenus.remove(orderMenu);
                break;
            }
        }
    }

    @Override
    public void order() {
        orderRepository.save(order);
        for (OrderMenu orderMenu : orderMenus) {
            orderMenuRepository.save(orderMenu);
        }
    }

    @Override
    public void resetOrder() {
        order = new Order();
        orderMenus = new HashSet<>();
    }

    @Override
    public Order getOrder() {
        return order;
    }

    @Override
    public void setTableNumber(int tableNumber) {
        order.setTableNumber(tableNumber);
    }

    @Override
    public Set<OrderMenu> getOrderMenus() {
        return orderMenus;
    }

    @Override
    public int getTotalPrice() {
        return order.getTotalPrice();
    }

    @Override
    public void calculateTotalPrice() {
        int totalPrice = 0;
        for (OrderMenu orderMenu: orderMenus) {
            totalPrice += orderMenu.getMenu().getPrice() * orderMenu.getQuantity();
        }

        order.setTotalPrice(totalPrice);
    }
}
