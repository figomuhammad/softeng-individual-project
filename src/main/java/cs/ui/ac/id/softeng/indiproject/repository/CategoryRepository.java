package cs.ui.ac.id.softeng.indiproject.repository;

import cs.ui.ac.id.softeng.indiproject.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {
    Category findById(long id);
    Category findByName(String name);
}
