package cs.ui.ac.id.softeng.indiproject.service;

public interface AuthService {
    void generateAuthKey();
    String getAuthKey();
    boolean checkAuthKey(String enteredKey);
}

