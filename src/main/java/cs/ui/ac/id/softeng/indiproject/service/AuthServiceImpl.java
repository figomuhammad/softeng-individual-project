package cs.ui.ac.id.softeng.indiproject.service;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class AuthServiceImpl implements AuthService {

    private String authKey;
    private static final int length = 6;

    @Override
    @Scheduled(fixedRate = 300000)
    public void generateAuthKey() {
        String numbers = "0123456789";
        Random random = new Random();

        StringBuilder authKey = new StringBuilder();

        for (int i = 0; i < length; i++) {
            authKey.append(numbers.charAt(random.nextInt(numbers.length())));
        }

        this.authKey = authKey.toString();
    }

    @Override
    public String getAuthKey() {
        return authKey;
    }

    @Override
    public boolean checkAuthKey(String enteredKey) {
        return authKey.equals(enteredKey);
    }
}
