package cs.ui.ac.id.softeng.indiproject.repository;

import cs.ui.ac.id.softeng.indiproject.model.Menu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MenuRepository extends JpaRepository<Menu, Long> {
    Menu findById(long id);
}
