package cs.ui.ac.id.softeng.indiproject.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Setter @Getter
@NoArgsConstructor
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private int tableNumber;

    private int totalPrice;

    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL)
    private Set<OrderMenu> orderMenu;

    public void addOrderMenu(OrderMenu orderMenu) {
        this.orderMenu.add(orderMenu);
    }

    public void removeOrderMenu(OrderMenu orderMenu) {
        this.orderMenu.remove(orderMenu);
    }
}
