package cs.ui.ac.id.softeng.indiproject;

import cs.ui.ac.id.softeng.indiproject.model.Category;
import cs.ui.ac.id.softeng.indiproject.model.Menu;
import cs.ui.ac.id.softeng.indiproject.model.Photo;
import cs.ui.ac.id.softeng.indiproject.repository.CategoryRepository;
import cs.ui.ac.id.softeng.indiproject.repository.MenuRepository;
import cs.ui.ac.id.softeng.indiproject.repository.PhotoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class DataInitializer {

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private MenuRepository menuRepository;

    @Autowired
    private PhotoRepository photoRepository;

    @PostConstruct
    public void init() {
        Category category = new Category("Food");
        categoryRepository.save(category);

        Photo photo = new Photo(
                "Beef Lasagna",
                "https://www.simplyrecipes.com/wp-content/uploads/2012/11/Vegetarian-Lasagna-LEAD-1.jpg"
                );
        photoRepository.save(photo);

        Menu menu = new Menu(
                "Beef Lasagna",
                "A flat and expanded pasta sheet, traditionally made in Italy with Parmigiano-Reggiano (Parmesan cheese), Béchamel sauce (white sauce), and ragù (a meat-based sauce)",
                50000,
                photoRepository.findByName("Beef Lasagna"),
                categoryRepository.findByName("Food")
                );
        menuRepository.save(menu);

        photo = new Photo(
                "Spaghetti",
                "https://img.kidspot.com.au/ahjA2J7c/w643-h428-cfill-q90/kk/2018/04/722-503782-1.jpg"
        );
        photoRepository.save(photo);

        menu = new Menu(
                "Spaghetti",
                "Spaghetti is a long, thin, solid, cylindrical pasta. It is a staple food of traditional Italian cuisine.",
                250000,
                photoRepository.findByName("Spaghetti"),
                categoryRepository.findByName("Food")
        );
        menuRepository.save(menu);
    }
}
