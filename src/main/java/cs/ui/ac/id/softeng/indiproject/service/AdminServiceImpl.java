package cs.ui.ac.id.softeng.indiproject.service;

import cs.ui.ac.id.softeng.indiproject.repository.CategoryRepository;
import cs.ui.ac.id.softeng.indiproject.repository.MenuRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdminServiceImpl implements AdminService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private MenuRepository menuRepository;


    @Override
    public void deleteCategory(long id) {
        categoryRepository.deleteById(id);
    }

    @Override
    public void deleteMenu(long id) {
        menuRepository.deleteById(id);
    }
}
