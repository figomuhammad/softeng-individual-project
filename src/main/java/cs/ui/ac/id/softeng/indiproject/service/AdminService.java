package cs.ui.ac.id.softeng.indiproject.service;

public interface AdminService {
    void deleteCategory(long id);
    void deleteMenu(long id);
}
