package cs.ui.ac.id.softeng.indiproject.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Setter @Getter
@NoArgsConstructor
@Table(name = "photos")
public class Photo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;

    private String url;

    @OneToOne(mappedBy = "photo")
    private Menu menu;

    public Photo(String name, String url) {
        this.name = name;
        this.url = url;
    }
}
