package cs.ui.ac.id.softeng.indiproject.service;

import cs.ui.ac.id.softeng.indiproject.model.Menu;

import java.util.List;

public interface IndexService {
    List<Menu> getFeaturedMenus();
}
