package cs.ui.ac.id.softeng.indiproject.controller;

import cs.ui.ac.id.softeng.indiproject.model.Menu;
import cs.ui.ac.id.softeng.indiproject.model.Photo;
import cs.ui.ac.id.softeng.indiproject.repository.CategoryRepository;
import cs.ui.ac.id.softeng.indiproject.repository.MenuRepository;
import cs.ui.ac.id.softeng.indiproject.repository.PhotoRepository;
import cs.ui.ac.id.softeng.indiproject.service.AdminService;
import cs.ui.ac.id.softeng.indiproject.model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private AdminService adminService;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private MenuRepository menuRepository;

    @Autowired
    private PhotoRepository photoRepository;

    @GetMapping("/")
    public String dashboard(Model model) {

        return "admin";
    }

    @GetMapping("/menus")
    public String menus(Model model) {
        model.addAttribute("categories", categoryRepository.findAll());

        return "admin-menus";
    }

    @PostMapping("/menus/category/delete")
    public String deleteCategory(@RequestParam(name = "category-id") long id) {
        adminService.deleteCategory(id);

        return "redirect:/admin/menus";
    }

    @PostMapping("/menus/menu/delete")
    public String deleteMenu(@RequestParam(name = "menu-id") long id) {
        adminService.deleteMenu(id);

        return "redirect:/admin/menus";
    }

    @GetMapping("menus/category/add")
    public String addCategoryGet() {
        return "admin-menus-category-add";
    }

    @PostMapping("menus/category/add")
    public String addCategoryPost(@RequestParam(name = "name") String name) {
        Category category = new Category(name);
        categoryRepository.save(category);

        return "redirect:/admin/menus";
    }

    @GetMapping("menus/menu/add")
    public String addMenuGet(Model model) {
        model.addAttribute("categories", categoryRepository.findAll());

        return "admin-menus-menu-add";
    }

    @PostMapping("menus/menu/add")
    public String addMenuPost(@RequestParam(name = "name") String name,
                              @RequestParam(name = "description") String description,
                              @RequestParam(name = "price") int price,
                              @RequestParam(name = "category-id") long categoryId,
                              @RequestParam(name = "photo-url") String photoUrl) {
        Category category = categoryRepository.findById(categoryId);
        Photo photo = new Photo(name, photoUrl);
        photoRepository.save(photo);

        Menu menu = new Menu(name, description, price, photoRepository.findByName(name), category);
        menuRepository.save(menu);

        return "redirect:/admin/menus";
    }
}
