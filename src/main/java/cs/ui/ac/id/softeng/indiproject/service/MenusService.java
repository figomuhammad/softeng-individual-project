package cs.ui.ac.id.softeng.indiproject.service;

import cs.ui.ac.id.softeng.indiproject.model.Order;
import cs.ui.ac.id.softeng.indiproject.model.OrderMenu;

import java.util.Set;

public interface MenusService {
    void addMenu(long id, int quantity);
    void deleteMenu(long id);
    void order();
    void resetOrder();
    Order getOrder();
    void setTableNumber(int tableNumber);
    Set<OrderMenu> getOrderMenus();
    int getTotalPrice();
    void calculateTotalPrice();
}
