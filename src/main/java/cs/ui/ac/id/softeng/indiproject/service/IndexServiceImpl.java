package cs.ui.ac.id.softeng.indiproject.service;

import cs.ui.ac.id.softeng.indiproject.model.Menu;
import cs.ui.ac.id.softeng.indiproject.repository.MenuRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class IndexServiceImpl implements IndexService {

    @Autowired
    private MenuRepository menuRepository;

    @Override
    public List<Menu> getFeaturedMenus() {
        // TODO
        List<Menu> menus = menuRepository.findAll();
        List<Menu> featuredMenus = new ArrayList<>();

        for(int i=0; i<menus.size(); i++) {
            featuredMenus.add(menus.get(i));
            if (i > 3) break;
        }

        return featuredMenus;
    }
}
