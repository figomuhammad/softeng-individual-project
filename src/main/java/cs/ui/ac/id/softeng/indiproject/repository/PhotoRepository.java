package cs.ui.ac.id.softeng.indiproject.repository;

import cs.ui.ac.id.softeng.indiproject.model.Photo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PhotoRepository extends JpaRepository<Photo, Long> {
    Photo findByName(String name);
}
