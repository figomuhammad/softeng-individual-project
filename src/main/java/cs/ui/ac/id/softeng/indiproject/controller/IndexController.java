package cs.ui.ac.id.softeng.indiproject.controller;

import cs.ui.ac.id.softeng.indiproject.model.Menu;
import cs.ui.ac.id.softeng.indiproject.repository.MenuRepository;
import cs.ui.ac.id.softeng.indiproject.service.IndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class IndexController {

    @Autowired
    private IndexService indexService;

    @GetMapping("")
    public String index(Model model) {
        //TODO
        List<Menu> featuredMenus = indexService.getFeaturedMenus();
        model.addAttribute("featuredMenus", featuredMenus);

        return "index";
    }
}
