package cs.ui.ac.id.softeng.indiproject.controller;

import cs.ui.ac.id.softeng.indiproject.service.AuthService;
import cs.ui.ac.id.softeng.indiproject.repository.CategoryRepository;
import cs.ui.ac.id.softeng.indiproject.repository.OrderRepository;
import cs.ui.ac.id.softeng.indiproject.service.MenusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MenusController {

    @Autowired
    private MenusService menusService;

    @Autowired
    private AuthService authService;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private OrderRepository orderRepository;

    @GetMapping("/menus")
    public String menus(Model model) {
        model.addAttribute("categories", categoryRepository.findAll());
        model.addAttribute("orderMenus", menusService.getOrderMenus());

        return "menus";
    }

    @PostMapping("/menus/add")
    public String addMenu(@RequestParam(name = "menu-id") long id,
                          @RequestParam(name = "quantity") int quantity) {
        menusService.addMenu(id, quantity);

        return "redirect:/menus";
    }

    @PostMapping("/menus/delete")
    public String deleteMenu(@RequestParam(name = "order-menu-id") long id) {
        menusService.deleteMenu(id);

        return "redirect:/menus";
    }

    @GetMapping("/menus/summary")
    public String summary(Model model, String error) {
        menusService.calculateTotalPrice();

        model.addAttribute("orderMenus", menusService.getOrderMenus());
        model.addAttribute("totalPrice", menusService.getTotalPrice());

        if (error != null) {
            model.addAttribute("error", "Invalid authentication key");
        }

        return "menus-summary";
    }

    @PostMapping("/menus/order")
    public String order(@RequestParam(name = "table-number") int tableNumber,
                        @RequestParam(name = "auth-key") String authKey) {

        if (authService.checkAuthKey(authKey)) {
            menusService.setTableNumber(tableNumber);
            menusService.order();

            return "redirect:/menus/order/cash";
        } else {
            return "redirect:/menus/summary?error";
        }
    }

    @GetMapping("/menus/order/cash")
    public String payCash(Model model) {
        model.addAttribute("order", menusService.getOrder());
        menusService.resetOrder();

        return "menus-order-cash";
    }
}
