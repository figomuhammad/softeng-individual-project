package cs.ui.ac.id.softeng.indiproject.controller;

import cs.ui.ac.id.softeng.indiproject.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class AuthKeyController {

    @Autowired
    AuthService authService;

    @GetMapping("/key")
    @ResponseBody
    public String showKey() {
        return authService.getAuthKey();
    }
}
